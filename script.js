let container = document.querySelector(".content");
let wrap_box = document.querySelector('.wrap-box')




axios.get("https://jsonplaceholder.typicode.com/photos").then((res) => {
  if (res.status === 200 || res.status === 201) {
    let data = res.data.slice(0, 4);
    reload(data, container);
    console.log("5", data);
    let dataSecond = res.data.slice(0, 24);
    reloadTwo(dataSecond, wrap_box)
  }
});

function reload(params, box) {
  box.innerHTML = "";
  for (let item of params) {
    let itemm = document.createElement("div");
    let itemTitle = document.createElement("span");
    let itemImg = document.createElement("img");

    itemm.classList.add("item");
    itemTitle.innerHTML = item.title;
    itemImg.src = item.url;

    itemm.append(itemImg);
    itemm.append(itemTitle);
    box.append(itemm);
  }
}
function reloadTwo(params, box) {
  box.innerHTML = "";
  for (let item of params) {
    let itemImg = document.createElement("img");

    itemImg.src = item.url;
   
    box.append(itemImg);
  }
}
